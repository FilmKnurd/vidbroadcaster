$(document).ready(function(){
	_wq = window._wq || [];
	config = window.config || {};
	wqManager = {};

	$('.wistia_embed').addClass('wistia_async_' + config.videoId);
	$('#countdown').countdown(new Date(config.startTime), function(event) {
		$(this).text(event.strftime('%-H:%M:%S'));
	});

	/**
	 * Wistia Controller
	 *
	 * Binds to wistia video events and state manager events and uses
	 * the panel manager to hide/show the appropriate sections.
	 */
	wqManager[config.videoId] = function(video) {
		var startTime = new Date(config.startTime).getTime();
		var duration = video.duration() * 1000;
		var endTime = startTime + duration;
		var controller = stateManager(startTime, endTime, config.cta_events);
		var isPlaying = false;
		var isPlayable = false;

		// Disable pause
		video.bind('pause', function() {
			if (!isPlaying) { return; }

			video.play();
		});

		video.bind('play', function() {
			// Prevent clicking on video to play
			if (!isPlayable) {
				video.pause();
				video.time(0);
				return;
			}

			isPlaying = true;
		});

		video.bind('end', function() {
			isPlaying = false;
			isPlayable = false;

			panelManager.showPostShow();
		});

		$(controller).on('pre', function() {
			panelManager.showPreShow();
		});

		$(controller).on('play', function() {
			var now = new Date().getTime();
			var seekTime = (now - startTime) / 1000;

			isPlayable = true;
			video.time(seekTime);
			video.play();
			panelManager.showMainEvent();
		});

		$(controller).on('stop', function() {
			panelManager.showPostShow();
		});

		$(controller).on('cta', function(e, id) {
			panelManager.enableCTA(id);
		});

		// Start checking the time and managing events
		controller.start();
	};



	// Initialize Wistia and get the ball rolling
	_wq.push(wqManager);



	/*****************************************************************************************
	 * UTILITIES
	 ****************************************************************************************/

	/**
	* Panel Manager
	*
	* Helper methods to control displaying sections on the page.
	*/
	var panelManager = {
		panels: $('.panel'),
		preShowPanel: $('#pre-show'),
		mainEventPanel: $('#main-event'),
		postShowPanel: $('#post-show'),

		enableCTA: function(id) {
			var $cta = $(id + '.inactive');

			if ($cta.length === 0) { return; } // It's already active

			$cta.removeClass('inactive');
			$cta.addClass('active');
		},

		showPreShow: function() {
			this.hidePanels();
			this.preShowPanel.removeClass('hidden');
		},

		showMainEvent: function() {
			this.hidePanels();
			this.mainEventPanel.removeClass('hidden');
		},

		showPostShow: function() {
			this.hidePanels();
			this.postShowPanel.removeClass('hidden');
		},

		hidePanels: function () {
			this.panels.each(function() {
				var panel = $(this);
				if (!panel.hasClass('hidden')) {
					panel.addClass('hidden');
				}
			});
		}
	};

	/**
	 * State Manager
	 *
	 * Manages the timing of events. Every 100ms it checks the time and triggers
	 * the appropriate action given the current state and time.
	 *
	 * Events:
	 *   - Pre: Initial state if page is loaded before start time.
	 *   - Play: Start time has passed
	 *   - Stop: End time has passed
	 *   - CTA: Elapsed time has passed a cta marker
	 */
	var stateManager = function(start, end, cta_events) {
		var _controller = {};
		var _state = 'unstarted';
		var _stateTimerId;
		var events = cta_events;
		var endTime = end;
		var startTime  = start;

		/**
		 * Trigger CTA action if elapsed time exceeds the given marker
		 */
		function checkEvents() {
			var now = new Date().getTime();
			var elapsedTime = (now - startTime) / 1000;

			events.forEach(function(event) {
				if (elapsedTime > event.time) {
					$(_controller).trigger('cta', event.id);
				}
			});
		}

		/**
		 * Trigger the appropriate video action based on the current time
		 * in relation to the start and end times.
		 */
		function checkState() {
			var now = new Date().getTime();
			var hasNotStartedYet = now < startTime;
			var hasEnded = now > endTime;

			if (hasNotStartedYet) { return 'unstarted'; }
			if (hasEnded) { return 'ended'; }

			return 'playable';
		}

		/**
		 * Before the periodic checking begins, trigger the initial state
		 */
		function triggerInitialEvent() {
			_state = checkState();
			if (_state === 'unstarted') { $(_controller).trigger('pre'); }
			if (_state === 'playable') { $(_controller).trigger('play'); }
			checkEvents();
		}

		/**
		 * State machine to decide the appropriate action given the current state
		 * and the new state.
		 */
		function updateState() {
			var currentState = _state;
			var nextState = checkState();

			if (nextState === 'ended') {
				$(_controller).trigger('stop');
				clearInterval(_stateTimerId);
			}

			if (currentState === 'unstarted' && nextState === 'playable') {
				$(_controller).trigger('play');
			}

			if (currentState === 'playable' && nextState === 'playable') {
				checkEvents();
			}

			_state = nextState;
		}

		/**
		 * Public API method to kick off perodic state management
		 */
		_controller.start = function() {
			triggerInitialEvent();
			_stateTimerId = setInterval(updateState, 100);
		};

		return _controller;
	};
});


