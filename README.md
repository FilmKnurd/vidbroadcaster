# Video Broadcaster

Simulates a live broadcast with a pre-recorded video hosted at Wistia.

## Configuration

Edit `config.js` to setup the broadcaster

```JS
var config = {
	videoId: 'c8ebykvh27',
	startTime: 'January 20, 2016 11:18:00',
	cta_events: [{
		id: '#cta-1',
		time: 30
	}, {
		id: '#cta-2',
		time: 60
	}]
};
```

- `videoId` The Wistia hash ID for the video
- `startTime` The date and time the event should start. Format: Month DD, YYYY HH:MM:SS
- `cta_events` An array of objects to indicate which call to actions should be triggered at which times. Each call to action consists of: `id`, which is the id of the div that should be displayed preceded by an octothorp. `time` the number of elasped seconds after the video begins when this cta should be activated.

## Hosting

Put these files anywhere static assets can be served. To test locally, start a simple web server inside this folder:

```
$ python -m SimpleHTTPServer 8000
```

Then go to `http://localhost:8000` in your browser.
